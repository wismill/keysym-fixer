#!/usr/bin/env python3

import argparse
from enum import Enum, unique
from functools import partial
import glob
from io import TextIOWrapper
import os
import platform
from pathlib import Path
import re
import shutil
import sys
import tempfile
from typing import Generator, Sequence

if platform.system() != "Linux":
    raise ValueError(f"Unsupported OS: {platform.system()}")

XKB_KEY_PATTERN = re.compile(
    r"""^(?P<before>\s*key\s+<[^>]+>\s*\{)(?P<body>[^}]+)(?=\})""", re.VERBOSE
)
COMPOSE_LHS_PATTERN = re.compile(
    r"""^(?P<before>\s*)(?P<body>(?:<[A-Za-z0-9_]+>\s*)+:)""", re.VERBOSE
)
UNICODE_KEYSYM_PATTERN = re.compile(r"""(?<!\w)(?P<keysym>U[0-9a-fA-F]{1,6})(?!\w)""")
KEYSYMS_SUBSTITUTIONS = (
    "dead_a",
    "dead_A",
    "dead_e",
    "dead_E",
    "dead_i",
    "dead_I",
    "dead_o",
    "dead_O",
    "dead_u",
    "dead_U",
    "dead_small_schwa",
    "dead_capital_schwa",
)
# Non-characters used to encode custom dead key keysyms
DEFAULT_KEYSYMS = tuple(
    f"U{cp:X}"
    for cp in range(0xFDD0, min(0xFDEF, 0xFDD0 + len(KEYSYMS_SUBSTITUTIONS) - 1) + 1)
)


@unique
class FileType(Enum):
    XKB = XKB_KEY_PATTERN
    Compose = COMPOSE_LHS_PATTERN


def get_default_files():
    if compose_file := os.getenv("XCOMPOSEFILE"):
        yield Path(compose_file)
    yield Path("~/.XCompose").expanduser()
    yield from glob.glob("/usr/share/X11/locale/**/Compose")
    yield from glob.glob("/usr/share/X11/xkb/**/*")


def check_file(keysyms: Sequence[str], type: FileType, path: Path) -> bool:
    regex: re.Pattern = type.value
    found = False
    with path.open("rt", encoding="UTF-8") as fd:
        for line_nbr, line in enumerate(fd):
            if m := regex.match(line):
                string = m.group("body")
                if ms := UNICODE_KEYSYM_PATTERN.findall(string):
                    if keysymsʹ := set(k for k in ms if k in keysyms):
                        found = True
                        print(
                            f"""Found at line {line_nbr}: {", ".join(keysymsʹ)}""",
                            file=sys.stderr,
                        )
    return found


def find(
    keysyms: Sequence[str], inputs: list[Path]
) -> Generator[tuple[Path, FileType], None, None]:
    print(f"""* Looking for keysyms: {", ".join(keysyms)}""", file=sys.stderr)
    for path in inputs:
        try:
            path = path.resolve(strict=True)
        except FileNotFoundError:
            print(f"* Skipping invalid path: {path}", file=sys.stderr)
            continue
        if not path.is_file():
            print(f"* Skipping non regular file: {path}", file=sys.stderr)
            continue
        path_str = str(path)
        if "xkb" in path_str or "symbols" in path_str:
            type = FileType.XKB
        else:
            type = FileType.Compose
        print(f"* Checking {type.name} file: {path}", file=sys.stderr)
        if check_file(keysyms, type, path):
            print(str(path))
            yield path, type


def get_inputs(args: argparse.Namespace) -> list[Path]:
    inputs: list[Path]
    if not (inputs := args.input) or args.default_paths:
        inputs.extend(map(Path, get_default_files()))
    return inputs


def check(args: argparse.Namespace):
    inputs = get_inputs(args)
    keysyms = args.keysym
    paths = tuple(find(keysyms, inputs))
    if paths:
        print("-> Found potentially problematic dead keys", file=sys.stderr)
    else:
        print("-> No potentially problematic dead keys found", file=sys.stderr)


def replace_keysym(keysyms_dict: dict[str, str], m: re.Match[str]) -> str:
    return keysyms_dict.get(m.group("keysym"), m.group("keysym"))


def replace_line(keysyms_dict: dict[str, str], m: re.Match[str]) -> str:
    return m.group("before") + UNICODE_KEYSYM_PATTERN.sub(
        partial(replace_keysym, keysyms_dict), m.group("body")
    )


def fix_file(
    keysyms_dict: dict[str, str], type: FileType, ifd: TextIOWrapper, ofd: TextIOWrapper
):
    regex: re.Pattern = type.value
    for line_nbr, line in enumerate(ifd):
        lineʹ = regex.sub(partial(replace_line, keysyms_dict), line)
        ofd.write(lineʹ)


def fix(args: argparse.Namespace):
    inputs = get_inputs(args)
    keysyms = args.keysym
    if len(keysyms) > len(KEYSYMS_SUBSTITUTIONS):
        raise ValueError("Too much keysyms")
    if conflicts := set(KEYSYMS_SUBSTITUTIONS).intersection(keysyms):
        raise ValueError(f"Keysyms conflicts: {conflicts}")
    keysyms_dict = dict(zip(keysyms, KEYSYMS_SUBSTITUTIONS))
    pathʹ: Path
    for path, type in find(keysyms, inputs):
        with tempfile.NamedTemporaryFile("wt", encoding="UTF-8") as ofd:
            with path.open("rt", encoding="UTF-8") as ifd:
                fix_file(keysyms_dict, type, ifd, ofd)
            ofd.flush()
            if args.inplace:
                pathʹ = path
                print(f"Overwriting file: {pathʹ}")
            else:
                pathʹ = args.output.joinpath(*path.parts[1:])
                pathʹ.parent.mkdir(parents=True, exist_ok=True)
                print(f"Writing fixed file: {pathʹ}")
            shutil.copyfile(ofd.file.name, pathʹ)


def add_common_arguments(parser: argparse.ArgumentParser):
    parser.add_argument("input", type=Path, nargs="*", help="Input file")
    parser.add_argument(
        "-d", "--default-paths", action="store_true", help="Add default paths"
    )
    parser.add_argument(
        "-k", "--keysym", nargs="*", help="Keysym", default=DEFAULT_KEYSYMS
    )


def make_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Detect and fix issues with keysym support"
    )
    subparsers = parser.add_subparsers()
    detect_parser = subparsers.add_parser(
        "check", help="Check for potentially problematic dead keys"
    )
    detect_parser.set_defaults(func=check)
    add_common_arguments(detect_parser)
    fix_parser = subparsers.add_parser(
        "fix", help="Fix potentially problematic dead keys"
    )
    g = fix_parser.add_mutually_exclusive_group(required=True)
    g.add_argument("-o", "--output", type=Path, help="Output path")
    g.add_argument("--inplace", action="store_true")
    fix_parser.set_defaults(func=fix)
    add_common_arguments(fix_parser)
    return parser


if __name__ == "__main__":
    parser = make_parser()
    args = parser.parse_args()
    args.func(args)
